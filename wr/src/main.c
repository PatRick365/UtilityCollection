#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>


int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "wr: missing operand\n");
        return 1;
    }

    if (argc > 3)
    {
        fprintf(stderr, "wr: to many operands\n");
        return 1;
    }

    // open file
    int fd = open(argv[1], O_CREAT | O_WRONLY | O_EXCL, S_IRUSR | S_IWUSR);
    if (fd < 0)
    {
        fprintf(stderr, "wr: %s\n", strerror(errno));
        return 1;
    }

    // write to file
    if (argc == 3)
    {
        unsigned long length = strlen(argv[2]);
        write(fd, argv[2], length);
        write(fd, "\n", 1);
    }

    close(fd);

    return 0;
}
