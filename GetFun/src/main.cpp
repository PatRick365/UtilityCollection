#include <string>
#include <iostream>
#include <termios.h>
#include <unistd.h>

#include <curl/curl.h>
#include "json.hpp"
#include "html_decode.hpp"

constexpr const char *progName = "getfun";
//constexpr const int versionMinor = 1;
//constexpr const int versionMajor = 0;

/// {"id":209,"type":"general","setup":"What do you call a crowd of chess players bragging about their wins in a hotel lobby?","punchline":"Chess nuts boasting in an open foyer."}
constexpr static const char *urlJoke = "https://official-joke-api.appspot.com/random_joke";

/// {"id":"42af198e-34fd-4b93-a474-3d8f0f1c204b","text":"Two-thirds of the world`s eggplant is grown in New Jersey. \u00a0","source":"djtech.net","source_url":"http:\/\/www.djtech.net\/humor\/useless_facts.htm","language":"en","permalink":"https:\/\/uselessfacts.jsph.pl\/42af198e-34fd-4b93-a474-3d8f0f1c204b"}
constexpr static const char *urlUselessFact = "https://uselessfacts.jsph.pl/random.json";

/// { "type": "success", "value": { "id": 284, "joke": "Chuck Norris' first job was as a paperboy. There were no survivors.", "categories": [] } }
constexpr static const char *urlChuckNorris = "http://api.icndb.com/jokes/random";


/// { "category": "Programming", "type": "single", "joke": "1. Open terminal\n2. mkdir snuts\n3. cd snuts", "id": 56 }
/// { "category": "Programming", "type": "twopart", "setup": ".NET developers are picky when it comes to food.", "delivery": "They only like Chicken NuGet.", "id": 49 }
constexpr static const char *urljokeProg = "https://sv443.net/jokeapi/category/Programming";


/* sleep until a key is pressed and return value. echo = 0 disables key echo. */
int keypress(unsigned char echo)
{
    struct termios savedState, newState;

    if (-1 == tcgetattr(STDIN_FILENO, &savedState))
    {
        return EOF;     /* error on tcgetattr */
    }

    newState = savedState;

    if ((echo = !echo)) /* yes i'm doing an assignment in an if clause */
    {
        echo = ECHO;    /* echo bit to disable echo */
    }

    /* disable canonical input and disable echo.  set minimal input to 1. */
    newState.c_lflag &= ~(echo | ICANON);
    newState.c_cc[VMIN] = 1;

    if (-1 == tcsetattr(STDIN_FILENO, TCSANOW, &newState))
    {
        return EOF;     /* error on tcsetattr */
    }

    int c = getchar();      /* block (without spinning) until we get a keypress */

    /* restore the saved state */
    if (-1 == tcsetattr(STDIN_FILENO, TCSANOW, &savedState))
    {
        return EOF;     /* error on tcsetattr */
    }
    return c;
}

size_t curlWriteCallback(void *contents, size_t size, size_t nmemb, std::string *str)
{
    size_t newLength = size * nmemb;
    try
    {
        //str->append((char*)contents, newLength);
        str->append(static_cast<char*>(contents), newLength);
    }
    catch(std::bad_alloc &e)
    {
        std::cout << "where: " << __PRETTY_FUNCTION__ << " - what: " << e.what() << std::endl;
        abort();
    }
    return newLength;
}

std::string getJsonAsString(const std::string &url)
{
    CURL *curl;
    CURLcode res;

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();

    std::string str;

    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L); //only for https
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L); //only for https
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &str);
    }
    else
    {
        std::cerr << "curl init failed...\n";
        abort();
    }

    res = curl_easy_perform(curl);

    if(res != CURLE_OK)
    {
        std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << "\n";
    }

    std::string str2 = str;

    curl_easy_cleanup(curl);

    return str2;
}

/// {"id":209,"type":"general","setup":"What do you call a crowd of chess players bragging about their wins in a hotel lobby?","punchline":"Chess nuts boasting in an open foyer."}
std::vector<std::string> parseJsonJoke(const std::string &jsonStr)
{
    nlohmann::json json = nlohmann::json::parse(jsonStr);

    std::vector<std::string> res;

    res.push_back(json.at("setup"));
    res.push_back(json.at("punchline"));

    return res;
}

/// {"id":"42af198e-34fd-4b93-a474-3d8f0f1c204b","text":"Two-thirds of the world`s eggplant is grown in New Jersey. \u00a0","source":"djtech.net","source_url":"http:\/\/www.djtech.net\/humor\/useless_facts.htm","language":"en","permalink":"https:\/\/uselessfacts.jsph.pl\/42af198e-34fd-4b93-a474-3d8f0f1c204b"}
std::vector<std::string> parseJsonUselessFact(const std::string &jsonStr)
{
    nlohmann::json json = nlohmann::json::parse(jsonStr);

    std::string res(json.at("text"));

    res.append("\nsource: ");
    res.append(json.at("source"));

    return { res };
}

/// { "type": "success", "value": { "id": 284, "joke": "Chuck Norris' first job was as a paperboy. There were no survivors.", "categories": [] } }
std::vector<std::string> parseJsonChuckNorrisFact(const std::string &jsonStr)
{
    nlohmann::json json = nlohmann::json::parse(jsonStr);

    nlohmann::json jsonValue = json.at("value");

    std::string encoded = jsonValue.at("joke");

    HtmlEncode::unescapeHtml(encoded);

    return { encoded };
}

/// { "category": "Programming", "type": "single", "joke": "1. Open terminal\n2. mkdir snuts\n3. cd snuts", "id": 56 }
/// { "category": "Programming", "type": "twopart", "setup": ".NET developers are picky when it comes to food.", "delivery": "They only like Chicken NuGet.", "id": 49 }
std::vector<std::string> parseJsonJokeProg(const std::string &jsonStr)
{
    nlohmann::json json = nlohmann::json::parse(jsonStr);

    if (json.at("type") == "single")
        return { json.at("joke") };

    return { json.at("setup"), json.at("delivery") };
}

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << progName << " [option]\n"
                  << "    -j get joke\n"
                  << "    -f get useless fact\n"
                  << "    -n get chuck norris fact\n"
                  << "    -p get programming joke\n";
        return 1;
    }

    std::string arg(argv[1]);

    std::vector<std::string> jokeStrVec;

    if (arg == "-j")
    {
        jokeStrVec = parseJsonJoke(getJsonAsString(urlJoke));
    }
    else if (arg == "-f")
    {
        jokeStrVec = parseJsonUselessFact(getJsonAsString(urlUselessFact));
    }
    else if (arg == "-n")
    {
        jokeStrVec = parseJsonChuckNorrisFact(getJsonAsString(urlChuckNorris));
    }
    else if (arg == "-p")
    {
        jokeStrVec = parseJsonJokeProg(getJsonAsString(urljokeProg));
    }
    else
    {
        std::cerr << "invalid option: '" << arg << "'\n";
        return 1;
    }

    for (size_t i = 0; i < jokeStrVec.size(); ++i)
    {
        if (i > 0)
        {
            keypress(0);
        }

        std::cout << jokeStrVec[i] << "\n";
    }

    return 0;
}
