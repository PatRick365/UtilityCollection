#pragma once

#include <termios.h>
#include <unistd.h>
#include <stdio.h>

namespace ConsoleUtil
{

/* sleep until a key is pressed and return value. echo = 0 disables key echo. */
int keypress()
{
    struct termios savedState;

    if (tcgetattr(STDIN_FILENO, &savedState) == -1)
    {
        return EOF;     /* error on tcgetattr */
    }

    struct termios newState = savedState;

    /* disable canonical input and disable echo.  set minimal input to 1. */
    newState.c_lflag &= ~(ECHO | ICANON);
    newState.c_cc[VMIN] = 1;

    if (tcsetattr(STDIN_FILENO, TCSANOW, &newState) == -1)
    {
        return EOF;     /* error on tcsetattr */
    }

    int c = getchar();      /* block (without spinning) until we get a keypress */

    /* restore the saved state */
    if (tcsetattr(STDIN_FILENO, TCSANOW, &savedState) == -1)
    {
        return EOF;     /* error on tcsetattr */
    }
    return c;
}

}
