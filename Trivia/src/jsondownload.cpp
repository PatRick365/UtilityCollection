#include "jsondownload.h"

#include <curl/curl.h>

#include <iostream>


namespace JsonDownload
{

size_t curlWriteCallback(void *contents, size_t size, size_t nmemb, std::string *str)
{
    size_t newLength = size * nmemb;
    try
    {
        str->append(static_cast<char *>(contents), newLength);
    }
    catch(std::bad_alloc &e)
    {
        std::cerr << e.what() << std::endl;
        exit(1);
    }
    return newLength;
}

std::string getJsonAsString(const std::string &url)
{
    curl_global_init(CURL_GLOBAL_DEFAULT);

    CURL *curl = curl_easy_init();

    std::string str;

    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L); //only for https
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L); //only for https
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &str);
    }
    else
    {
        std::cerr << "curl: init failed...\n";
        exit(1);
    }

    CURLcode res = curl_easy_perform(curl);

    if(res != CURLE_OK)
    {
        std::cerr << "curl: " << curl_easy_strerror(res) << "\n";
        exit(1);
    }

    curl_easy_cleanup(curl);

    return str;
}

}
