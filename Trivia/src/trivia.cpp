#include "trivia.h"
#include "html_decode.hpp"
#include "console_util.hpp"

#include <iostream>
#include <algorithm>
#include <random>

namespace TriviaHelpers
{

// {"response_code":0,"results":[{"category":"Entertainment: Video Games","type":"multiple","difficulty":"medium","question":"In vanilla Minecraft, you can make armor out of all BUT which of the following?","correct_answer":"Emeralds","incorrect_answers":["Diamonds","Iron","Leather"]}]}
// {"response_code":0,"results":[{"category":"Entertainment: Video Games","type":"boolean","difficulty":"medium","question":"In Half-Life 2, if you play the zombies&#039; speech in reverse, they actually speak coherent English.","correct_answer":"True","incorrect_answers":["False"]}]}
constexpr static const char *urlBase = "https://opentdb.com/api.php?amount=1";

constexpr std::array<Category, 24> categories
{
    {
        {-1,  -1, "Any Category" },
        { 0,   9, "General Knowledge" },
        { 1,  10, "Entertainment: Books" },
        { 2,  11, "Entertainment: Film" },
        { 3,  12, "Entertainment: Music" },
        { 4,  13, "Entertainment: Musicals & Theatres" },
        { 5,  14, "Entertainment: Television" },
        { 6,  15, "Entertainment: Video Games" },
        { 7,  16, "Entertainment: Board Games" },
        { 8,  18, "Science: Computers" },
        { 9,  19, "Science: Mathematics" },
        { 10, 20, "Mythology" },
        { 11, 21, "Sports" },
        { 12, 22, "Geography" },
        { 13, 23, "History" },
        { 14, 24, "Politics" },
        { 15, 25, "Art" },
        { 16, 26, "Celebrities" },
        { 17, 27, "Animals" },
        { 18, 28, "Vehicles" },
        { 19, 29, "Entertainment: Comics" },
        { 20, 30, "Science Gadgets" },
        { 21, 31, "Entertainment: Japanese Anime & Magma" },
        { 22, 32, "Entertainment: Cartoon & Animations" }
    }
};

constexpr std::array<Difficulty, 4> difficulties
{
    {
        { EnumDifficulty::eAnyDifficulty, "NONE" },
        { EnumDifficulty::eEasy,          "easy" },
        { EnumDifficulty::eMedium,        "medium" },
        { EnumDifficulty::eHard,          "hard" }
    }
};

std::string buildUrl(const Category &category, const Difficulty &difficulty)
{
    std::string result(urlBase);

    if (category.optionId >= 0)
    {
        result.append("&category=");
        result.append(std::to_string(category.apiId));
    }
    if (difficulty.m_enum != EnumDifficulty::eAnyDifficulty)
    {
        result.append("&difficulty=");
        result.append(difficulty.optionStr);
    }
    return result;
}

}

Trivia::Trivia(const nlohmann::json &json)
{
    if (json.at("response_code") != 0)
    {
        std::cerr << "api request was unsuccessful...\n";
        exit(1);
    }

    nlohmann::json results = json.at("results").at(0);

    m_question = results.at("question");
    HtmlEncode::unescapeHtml(m_question);

    TriviaHelpers::EnumType type;

    if (results.at("type") == "multiple")
        type = TriviaHelpers::EnumType::eMultiple;
    else if (results.at("type") == "boolean")
        type = TriviaHelpers::EnumType::eBoolean;
    else
    {
        std::cerr << "invalid type: " << results.at("type") << "?\n";
        exit(0);
    }

    switch (type)
    {
    case TriviaHelpers::EnumType::eBoolean:
    {
        m_answers = { "True", "False" };
        break;
    }
    case TriviaHelpers::EnumType::eMultiple:
    {
        nlohmann::json incorrectAnswers = results.at("incorrect_answers");

        m_answers = { results.at("correct_answer"),
                      incorrectAnswers.at(0),
                      incorrectAnswers.at(1),
                      incorrectAnswers.at(2) };

        std::random_device randDevice;
        auto rnd = std::default_random_engine(randDevice());
        std::shuffle(m_answers.begin(), m_answers.end(), rnd);
        break;
    }
    default:
    {
        std::cerr << "whaaaaat ???\n";
        exit(1);
    }
    }

    m_correctAnswerIndex = static_cast<size_t>(std::distance(m_answers.begin(), std::find(m_answers.begin(), m_answers.end(), results.at("correct_answer"))));

    for (auto &str : m_answers)
        HtmlEncode::unescapeHtml(str);

}

bool Trivia::run()
{
    std::cout << m_question << "\n";

    for (unsigned int i = 0; i < m_answers.size(); ++i)
    {
        std::cout << "  " << i + 1 << ") " << m_answers[i] << "\n";
    }

    int input;
    do
    {
        input = ConsoleUtil::keypress() - 48;
    }
    while (input < 1 || input > static_cast<int>(m_answers.size()));


    if (static_cast<unsigned int>(input - 1) == m_correctAnswerIndex)
    {
        std::cout << "CORRECT !! Answer: " << m_answers[m_correctAnswerIndex] << "\n";
        return true;
    }
    else
    {
        std::cout << "WRONG !! Answer: " << m_answers[m_correctAnswerIndex] << "\n";
        return false;
    }

}
