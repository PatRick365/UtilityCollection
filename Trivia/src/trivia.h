#pragma once

#include "json.hpp"

#include <array>
#include <string>

namespace TriviaHelpers
{

struct Category
{
    int optionId;
    int apiId;
    const char *description;
};


enum EnumDifficulty { eAnyDifficulty, eEasy, eMedium, eHard };
struct Difficulty
{
    EnumDifficulty m_enum;
    const char *optionStr;
};


enum EnumType { eAnyType, eMultiple, eBoolean };

extern const std::array<Category, 24> categories;
extern const std::array<Difficulty, 4> difficulties;

std::string buildUrl(const Category &category, const Difficulty &difficulty);

}


class Trivia
{
public:
    Trivia(const nlohmann::json &json);

    bool run();


private:
    TriviaHelpers::EnumType m_type;

    std::string m_question;

    std::vector<std::string> m_answers;
    size_t m_correctAnswerIndex;

};

