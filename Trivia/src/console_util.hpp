#pragma once

#include <iostream>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>


namespace ConsoleUtil
{

class AutoDisableCanonicalAndEcho
{
public:
    AutoDisableCanonicalAndEcho()
    {
        if (tcgetattr(STDIN_FILENO, &m_term) == -1)
        {
            std::cerr << "error: tcgetattr failed...";
        }

        m_isSet = true;

        struct termios newState = m_term;

        // disable canonical input and disable echo.  set minimal input to 1.
        newState.c_lflag &= ~(ECHO | ICANON);
        newState.c_cc[VMIN] = 1;

        if (tcsetattr(STDIN_FILENO, TCSANOW, &newState) == -1)
        {
            std::cerr << "error: tcsetattr failed...";
        }
    }
    ~AutoDisableCanonicalAndEcho()
    {
        if (!m_isSet)
            return;

        // restore the saved state
        if (tcsetattr(STDIN_FILENO, TCSANOW, &m_term) == -1)
        {
            std::cerr << "error: tcsetattr failed...";
        }
    }

private:
    bool m_isSet = false;
    struct termios m_term;
};

// sleep until a key is pressed and return value.
int keypress()
{
    AutoDisableCanonicalAndEcho restore;

    return getchar();   // block (without spinning) until we get a keypress
}

}
