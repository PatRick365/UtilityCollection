#pragma once

#include <string>

namespace JsonDownload
{

extern std::string getJsonAsString(const std::string &url);

}
