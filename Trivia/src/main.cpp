#include <string>
#include <iostream>
#include <memory>

#include <curl/curl.h>

#include "json.hpp"
#include "jsondownload.h"
#include "trivia.h"

constexpr const char *progName = "trivia";
//constexpr const int versionMinor = 1;
//constexpr const int versionMajor = 0;

int main(int argc, char **argv)
{
    std::vector<std::string> args;
    args.reserve(static_cast<size_t>(argc - 1));

    for (int i = 1; i < argc; ++i)
        args.push_back(argv[i]);

    if (args.size() == 1)
    {
        if (args.at(0) == "--help")
        {
            std::cout << "Usage: " << progName << " [options]\n"
                      << "  options:\n"
                      << "    difficulty:\n";

            for (unsigned int i = 1; i < TriviaHelpers::difficulties.size(); ++i)
            {
                std::cout << "     -"
                          << TriviaHelpers::difficulties[i].optionStr
                          << "\n";
            }

            std::cout << "    category:\n"
                      << "      -c[number]\n";
            for (unsigned int i = 1; i < TriviaHelpers::categories.size(); ++i)
            {
                std::cout << "        "
                          << TriviaHelpers::categories[i].optionId
                          << " - "
                          << TriviaHelpers::categories[i].description
                          << "\n";
            }
            return 0;
        }
    }

    std::unique_ptr<TriviaHelpers::Category> chosenCategory;
    std::unique_ptr<TriviaHelpers::Difficulty> chosenDifficulty;

    for (const std::string &arg : args)
    {
        bool validArg = false;
        if (arg.find("-c") == 0)
        {
            if (chosenCategory != nullptr)
            {
                std::cerr << "invalid otion: multiple categories set.\n";
                return 1;
            }

            int num = std::stoi(arg.substr(2));
            chosenCategory = std::make_unique<TriviaHelpers::Category>(TriviaHelpers::categories.at(static_cast<unsigned int>(num + 1)));
            validArg = true;
        }

        if (validArg)
            continue;

        for (const TriviaHelpers::Difficulty &difficulty : TriviaHelpers::difficulties)
        {
            if (arg == std::string("-") + difficulty.optionStr)
            {
                if (chosenDifficulty != nullptr)
                {
                    std::cerr << "invalid otion: multiple difficulties set.\n";
                    return 1;
                }
                chosenDifficulty = std::make_unique<TriviaHelpers::Difficulty>(difficulty);
                validArg = true;
                break;
            }
        }

        if (validArg)
            continue;

        std::cerr << "invalid otion: " << arg << "\n";
        return 1;
    }

    if (chosenCategory == nullptr)
        chosenCategory = std::make_unique<TriviaHelpers::Category>(TriviaHelpers::categories.front());

    if (chosenDifficulty == nullptr)
        chosenDifficulty = std::make_unique<TriviaHelpers::Difficulty>(TriviaHelpers::difficulties.front());

    std::string url = TriviaHelpers::buildUrl(*chosenCategory, *chosenDifficulty);
    std::string jsonStr = JsonDownload::getJsonAsString(url);

    Trivia trivia(nlohmann::json::parse(jsonStr));

    trivia.run();

    return 0;
}
