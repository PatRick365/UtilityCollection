#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <filesystem>
#include <iomanip>
#include <unistd.h>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <pwd.h>
#include <grp.h>

using namespace std::chrono;

namespace fs = std::filesystem;


constexpr const char *progName = "lsf";
//constexpr const int versionMinor = 1;
//constexpr const int versionMajor = 0;

const std::vector<std::string> FILE_SIZE_UNITS = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

constexpr unsigned int MAX_FILE_SIZE_SHOWN = 10000;
constexpr unsigned int EXEC_BUFFER_SIZE = 128;
constexpr unsigned int BYTE_UNIT_CONV_MULTI = 1024;
constexpr unsigned int EXERPT_SIZE = 100;


template <typename T>
std::string toStringWithPrecision(const T value, const int precision = 6)
{
    std::ostringstream out;
    out << std::setprecision(precision) << std::fixed << value;
    return out.str();
}

std::string formatFileSize(uintmax_t fileSize)
{
    long double bytes = fileSize;

    unsigned int exp = 0;

    while (bytes >= MAX_FILE_SIZE_SHOWN)
    {
        bytes /= BYTE_UNIT_CONV_MULTI;
        ++exp;
    }

    std::string unit = exp <= FILE_SIZE_UNITS.size() ? FILE_SIZE_UNITS.at(exp) : "?B";

    return toStringWithPrecision(bytes, 2) + " " + unit;


    //long double bytes = fileSize;


    /*while (bytes >= 10000)
    {
        bytes /= 1024;
        ++exp;
    }*/
    /*unsigned int exp = 0;
    for ( ; exp < FILE_SIZE_UNITS.size(); ++exp)
    {
        if (fileSize / static_cast<double>((exp * 1024)) < 10000)
            break;
    }
    --exp;

    std::string unit = exp <= FILE_SIZE_UNITS.size() ? FILE_SIZE_UNITS.at(exp) : "?B";

    return toStringWithPrecision(fileSize / static_cast<double>(exp * 1024), 2) + " " + unit;*/

    /*unsigned int exp = static_cast<unsigned int>(fileSize / 1024);

    std::log()

    double converted = fileSize / static_cast<double>(exp * 1024);

    std::string unit = exp <= FILE_SIZE_UNITS.size() ? FILE_SIZE_UNITS.at(exp) : "?B";

    std::cout << "exp: " << exp << "\n";

    return toStringWithPrecision(converted, 2) + " " + unit;*/
}

std::string exec(const char* cmd)
{
    std::array<char, EXEC_BUFFER_SIZE> buffer{};
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);

    if (!pipe)
        throw std::runtime_error("popen() failed!");

    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
        result += buffer.data();

    return result;
}

std::string readFile(const std::string &filePath, unsigned int charCount)
{
    std::fstream fin(filePath, std::fstream::in);

    char ch;
    unsigned int i = 0;
    std::string str;

    while (fin >> std::noskipws >> ch && i < charCount)
    {
        str.insert(str.end(), ch);
        i++;
    }
    return str;
}

unsigned short int getConsoleWidth()
{
    struct winsize winSize{};
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &winSize);
    return winSize.ws_col;
}

int run(const std::string &path)
{
    std::filesystem::path file(path);
    fs::directory_entry entry(file);

    if (!entry.exists())
    {
        std::cerr << progName << ": " << entry.path().filename().generic_string() << ": No such file\n";
        return 1;
    }

    if (entry.is_directory())
    {
        std::cerr << progName << ": " << entry.path().generic_string() << ": Is a directory\n";
        return 1;
    }

    //std::chrono::system_clock::to_time_t()
    auto tt = entry.last_write_time();
    //auto tp = (std::chrono::time_point)t;
    //std::chrono::system_clock::to_time_t();

    //std::time_t time = std::chrono::system_clock::to_time_t(entry.last_write_time());
    std::string absolutePath = fs::absolute(entry.path().generic_string());

    std::string fileType = exec(("file " + absolutePath).c_str());
    //std::string fileType = exec(std::string("\"file " + absolutePath + "\"").c_str());

    //std::string fileCommand = "file " + absolutePath;
    //std::replace(fileCommand.begin(), fileCommand.end(), " ", "\ ");
    //std::string fileType = exec(().c_str());
    fileType.erase(0, absolutePath.size() + 2);

    struct stat info{};
    stat(absolutePath.c_str(), &info);  // Error check omitted
    struct passwd *pw = getpwuid(info.st_uid);
    struct group  *gr = getgrgid(info.st_gid);

    std::string exerpt = readFile(absolutePath, EXERPT_SIZE);
    std::string exerptSize = std::to_string(exerpt.size());

    std::cout << "Path      : " << absolutePath << "\n";
    std::cout << "Type      : " << fileType;

    if (entry.is_regular_file())
        std::cout << "Size      : " << formatFileSize(entry.file_size()) << "\n";

    if (pw != nullptr)
        std::cout << "Owner     : " << pw->pw_name << "\n";

    if (gr != nullptr)
        std::cout << "Group     : " << gr->gr_name << "\n";

    std::cout << "Perms     : " << std::oct << static_cast<int>(entry.status().permissions()) << "\n";
    //std::cout << "Last Write: " << std::ctime(&time);

    if (entry.is_regular_file() && entry.file_size() > 0)
    {
        std::cout << "---Exerpt " << exerptSize << " chars:" + std::string(getConsoleWidth() - exerptSize.size() - 17, '-') + "\n";
        std::cout << exerpt << "\n";
        std::cout << std::string(getConsoleWidth(), '-') + "\n";
    }
    return 0;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        //std::cerr << "usage: lsf <path to file>\n";
        std::cerr << "Usage: " << progName << " [FILE]\n";
        return 1;
    }

    return run(argv[1]);
}
