cmake_minimum_required(VERSION 2.8.9)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

project(LsDirectory)

add_executable(lsd src/main.cpp)

target_link_libraries(lsd stdc++fs)

