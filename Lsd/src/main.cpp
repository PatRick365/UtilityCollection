#include <iostream>
#include <string>
#include <vector>
#include <filesystem>
#include <iomanip>
#include <unistd.h>
#include <sstream>
#include <fstream>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <pwd.h>
#include <grp.h>


namespace fs = std::filesystem;

constexpr const char *progName = "lsd";
//constexpr const int versionMinor = 1;
//constexpr const int versionMajor = 0;

const std::vector<std::string> FILE_SIZE_UNITS = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

constexpr unsigned int MAX_FILE_SIZE_SHOWN = 10000;
constexpr unsigned int EXEC_BUFFER_SIZE = 128;
constexpr unsigned int BYTE_UNIT_CONV_MULTI = 1024;


template <typename T>
std::string toStringWithPrecision(const T value, const int precision = 6)
{
    std::ostringstream out;
    out << std::setprecision(precision) << std::fixed << value;
    return out.str();
}

std::string format_file_size(uintmax_t fileSize)
{
    long double bytes = fileSize;

    unsigned int exp = 0;

    while (bytes >= MAX_FILE_SIZE_SHOWN)
    {
        bytes /= BYTE_UNIT_CONV_MULTI;
        ++exp;
    }

    std::string unit = exp <= FILE_SIZE_UNITS.size() ? FILE_SIZE_UNITS.at(exp) : "?B";

    return toStringWithPrecision(bytes, 2) + " " + unit;
}

std::string exec(const char* cmd)
{
    std::array<char, EXEC_BUFFER_SIZE> buffer{};
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);

    if (!pipe)
        throw std::runtime_error("popen() failed!");

    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
        result += buffer.data();

    return result;
}

unsigned short int getConsoleWidth()
{
    struct winsize winSize{};
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &winSize);
    return winSize.ws_col;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << progName << " [DIR]\n";
        return 1;
    }

    std::filesystem::path file(argv[1]);
    fs::directory_entry startEntry(file);

    if (!startEntry.exists())
    {
        std::cerr << progName << ": " << startEntry.path().filename().generic_string() << ": No such directory\n";
        return 1;
    }

    if (!startEntry.is_directory())
    {
        std::cerr << progName << ": " << startEntry.path().generic_string() << ": Is a not directory\n";
        return 1;
    }

    uintmax_t sizeTotal = 0;
    uintmax_t fileCount = 0;
    uintmax_t dirCount = 0;
    uintmax_t symLinkCount = 0;

    //fs::directory_options options = fs::skip_permission_denied;

    try
    {
        for (const auto &entry: fs::recursive_directory_iterator(startEntry.path()))
        {
            if (entry.is_symlink())
            {
                ++symLinkCount;
            }
            else if (entry.is_regular_file())
            {
                ++fileCount;
                sizeTotal += entry.file_size();
            }
            else if (entry.is_directory())
            {
                ++dirCount;
            }
        }
    }
    catch (std::exception& ex)
    {
        std::cerr << progName << ": " << ex.what() << "\n";
        return 1;
    }

    /// no work anymore with gcc /// std::time_t time = std::chrono::system_clock::to_time_t(startEntry.last_write_time());
    std::string absolutePath = fs::absolute(startEntry.path().generic_string());

    std::string fileType = exec(("file " + absolutePath).c_str());
    fileType.erase(0, absolutePath.size() + 2);

    struct stat info{};
    stat(absolutePath.c_str(), &info);  // Error check omitted
    struct passwd *pw = getpwuid(info.st_uid);
    struct group  *gr = getgrgid(info.st_gid);

    std::cout << "Path      : " << absolutePath << "\n";
    std::cout << "Type      : " << fileType;

    if (pw != nullptr)
        std::cout << "Owner     : " << pw->pw_name << "\n";

    if (gr != nullptr)
        std::cout << "Group     : " << gr->gr_name << "\n";

    std::cout << "Perms     : " << std::oct << static_cast<int>(startEntry.status().permissions()) << "\n";
    /// no work anymore with gcc /// std::cout << "Last Write: " << std::ctime(&time);

    std::cout << "\n";
    std::cout << "Sub dirs: " << dirCount << "\n";
    std::cout << "Symlinks: " << symLinkCount << "\n";
    std::cout << "Files   : " << fileCount << "\n";
    std::cout << "Size    : " << format_file_size(sizeTotal) << "\n";



    return 0;
}
